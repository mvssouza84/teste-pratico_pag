package br.com.marcus.pag.data.repository

import br.com.marcus.pag.BuildConfig
import br.com.marcus.pag.data.api.Api
import br.com.marcus.pag.data.model.MovieResult
import br.com.marcus.pag.data.model.Result
import br.com.marcus.pag.di.config.Database
import br.com.marcus.pag.domain.repository.MovieRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MovieRepositoryImpl(private val api: Api, private val database: Database) : MovieRepository {


    override fun loadMovies(): Flowable<MovieResult> {
        return api
            .loadMovies(BuildConfig.API_KEY)
            .observeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
    }

    override fun saveMovie(result: Result): Completable {
        return database
            .movieDao()
            .saveMovie(result)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
    }

    override fun loadMovieById(id: Int?): Single<Result> {
        return database
            .movieDao()
            .loadMovieById(id)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
    }

    override fun deleteMovie(result: Result): Completable {
        return database
            .movieDao()
            .deleteMovie(result)
            .observeOn(Schedulers.io())
            .subscribeOn(Schedulers.io())
    }

    override fun loadLocalMovies(): Flowable<List<Result>> {
        return database
            .movieDao()
            .loadMovies()
            .observeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
    }
}