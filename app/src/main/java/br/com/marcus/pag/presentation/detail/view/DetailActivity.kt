package br.com.marcus.pag.presentation.detail.view

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import br.com.marcus.pag.BuildConfig
import br.com.marcus.pag.R
import br.com.marcus.pag.extensions.formatDate
import br.com.marcus.pag.presentation.base.BaseActivity
import br.com.marcus.pag.presentation.detail.module.loadDetailModule
import br.com.marcus.pag.presentation.detail.state.DetailState
import br.com.marcus.pag.presentation.detail.viewmodel.DetailViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailActivity : BaseActivity() {

    private var result: br.com.marcus.pag.data.model.Result? = null
    private var menuItem: MenuItem? = null
    private val detailViewModel: DetailViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        init()
    }

    private fun init() {
        loadDetailModule()
        result = intent.getParcelableExtra("result")
        setupToolbar()
        showScreen()
        initObservers()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = result?.title
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_bar, menu)
        menuItem = menu?.findItem(R.id.menuFavorite)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuFavorite -> {
                saveMovie()
            }
            else -> super.onOptionsItemSelected(item)
        }
        return true
    }

    private fun showScreen() {
        Picasso.get().load(BuildConfig.IMAGE_URL + result?.backdropPath).into(activityDetailBanner)
        activityDetailMovieDesc.text =
            if (result?.overview?.isEmpty() == true) getString(R.string.no_overview) else result?.overview
        activityDetailMovieVote.text = result?.voteAverage.toString()
        activityDetailMovieDate.text = result?.releaseDate?.formatDate()
    }

    private fun initObservers() {
        detailViewModel.detailState.observe(this, Observer { state ->
            when (state) {
                is DetailState.SaveSuccess -> {
                    menuItem?.icon = ContextCompat.getDrawable(applicationContext, R.drawable.vector_fill_heart)
                    showToast("Filme salvo com sucesso")
                }

                is DetailState.SaveError -> {
                    showToast("Erro ao salvar filme")
                }

                is DetailState.RemoveSuccess -> {
                    menuItem?.icon = ContextCompat.getDrawable(applicationContext, R.drawable.vector_empty_heart)
                    showToast("Filme excluído com sucesso")
                }

                is DetailState.RemoveError -> {
                    showToast("Erro ao excluir filme")
                }

                is DetailState.ErrorGetLocalMovie -> {
                    showToast("Erro ao carregar local movie")
                }
            }
        })
    }

    private fun showToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }

    private fun saveMovie() {
        result?.let {
            detailViewModel.saveOrRemoveMovie(it)
        }
    }
}