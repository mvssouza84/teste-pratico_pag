package br.com.marcus.pag.presentation.detail.module

import br.com.marcus.pag.domain.usecase.DetailUseCase
import br.com.marcus.pag.presentation.detail.viewmodel.DetailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

internal val detailModule = module {
    factory { DetailUseCase(get()) }
    viewModel { DetailViewModel(get()) }
}

private val provideDetailModule = loadKoinModules(detailModule)
fun loadDetailModule() = provideDetailModule