package br.com.marcus.pag.domain.usecase

import br.com.marcus.pag.data.model.Result
import br.com.marcus.pag.domain.repository.MovieRepository
import io.reactivex.Flowable

class FavoriteUseCase(private val movieRepository: MovieRepository) {

    fun loadFavorites(): Flowable<List<Result>> {
        return movieRepository.loadLocalMovies()
    }
}