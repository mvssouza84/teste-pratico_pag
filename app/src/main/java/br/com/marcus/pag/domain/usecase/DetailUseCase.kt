package br.com.marcus.pag.domain.usecase

import br.com.marcus.pag.data.model.Result
import br.com.marcus.pag.domain.repository.MovieRepository
import io.reactivex.Completable
import io.reactivex.Single

class DetailUseCase(private val movieRepository: MovieRepository) {

    fun saveMovie(result: Result): Completable {
        return movieRepository.saveMovie(result)
    }

    fun getMovie(result: Result): Single<Result> {
        return movieRepository.loadMovieById(result.id)
    }

    fun removeMovie(result: Result): Completable {
        return movieRepository.deleteMovie(result)
    }
}