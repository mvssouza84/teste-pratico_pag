package br.com.marcus.pag.presentation.main.favorite.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.marcus.pag.R
import br.com.marcus.pag.data.model.Result
import br.com.marcus.pag.presentation.MovieResultListAdapter
import br.com.marcus.pag.presentation.base.BaseRecyclerAdapter
import br.com.marcus.pag.presentation.detail.view.DetailActivity
import br.com.marcus.pag.presentation.main.favorite.module.loadFavoriteModule
import br.com.marcus.pag.presentation.main.favorite.state.FavoriteState
import br.com.marcus.pag.presentation.main.favorite.viewmodel.FavoriteViewModel
import kotlinx.android.synthetic.main.fragment_favorite.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class FavoriteFragment : Fragment(), BaseRecyclerAdapter.OnItemClickListener {

    private val movieResultListAdapter by lazy { MovieResultListAdapter() }
    private val favoriteViewModel: FavoriteViewModel by viewModel()

    companion object {
        fun newInstance(): FavoriteFragment {
            return FavoriteFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorite, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        init()
    }

    override fun onItemClick(view: View, position: Int) {
        goToDetailActivity(movieResultListAdapter.recyclerList[position])
    }

    private fun goToDetailActivity(result: Result) {
        val intent = Intent(activity, DetailActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        intent.putExtra("result", result)
        startActivity(intent)
    }

    private fun init() {
        loadFavoriteModule()
        initObservers()
        setupRecycler()
        favoriteViewModel.loadFavorites()
    }

    private fun initObservers() {
        favoriteViewModel.favoriteState.observe(this, Observer { state ->
            when (state) {
                is FavoriteState.ShowLoading -> {
                    showLoading()
                    hideEmptyState()
                }

                is FavoriteState.HideLoading -> {
                    hideLoading()
                }

                is FavoriteState.ShowSuccess -> {
                    showSuccessScreen(state.resultList)
                    hideLoading()
                }

                is FavoriteState.ShowEmptyState -> {
                    showEmptyState()
                    hideLoading()
                }
            }
        })
    }

    private fun setupRecycler() {
        val linearLayoutManager = LinearLayoutManager(context)
        movieResultListAdapter.listener = this
        linearLayoutManager.orientation = RecyclerView.VERTICAL
        fragmentFavoriteRecycler.layoutManager = linearLayoutManager
        fragmentFavoriteRecycler.itemAnimator = DefaultItemAnimator()
        fragmentFavoriteRecycler.adapter = movieResultListAdapter
    }

    private fun showLoading() {
        fragmentFavoriteLoading.visibility = View.VISIBLE
        fragmentFavoriteRecycler.visibility = View.GONE
    }

    private fun hideLoading() {
        fragmentFavoriteLoading.visibility = View.GONE
    }

    private fun showSuccessScreen(results: List<Result>?) {
        fragmentFavoriteRecycler.visibility = View.VISIBLE
        movieResultListAdapter.addToList(results as ArrayList<Result>)
    }

    private fun showEmptyState() {
        emptyText.visibility = View.VISIBLE
    }

    private fun hideEmptyState() {
        emptyText.visibility = View.GONE
    }

    override fun onResume() {
        favoriteViewModel.loadFavorites()
        super.onResume()
    }
}