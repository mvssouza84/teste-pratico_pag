package br.com.marcus.pag.domain.repository

import br.com.marcus.pag.data.model.GenreResult
import io.reactivex.Flowable

interface GenreRepository {

    fun loadGenres(): Flowable<GenreResult>
}