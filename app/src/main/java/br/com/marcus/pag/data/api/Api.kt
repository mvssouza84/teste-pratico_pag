package br.com.marcus.pag.data.api

import br.com.marcus.pag.data.model.GenreResult
import br.com.marcus.pag.data.model.MovieResult
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("movie/upcoming")
    fun loadMovies(@Query("api_key") apiKey: String, @Query("language") language: String = "pt-BR", @Query("page") page: Int = 1): Flowable<MovieResult>

    @GET("genre/movie/list")
    fun loadGenres(@Query("api_key") apiKey: String, @Query("language") language: String = "pt-BR"): Flowable<GenreResult>

}