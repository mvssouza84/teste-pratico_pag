package br.com.marcus.pag.data.model

import com.squareup.moshi.Json

data class GenreResult(
    @field:Json(name = "genres") val genreList: List<Genre>
)

data class Genre(
    val id: Int,
    @field:Json(name = "name") val name: String
)