package br.com.marcus.pag.presentation.main.favorite.module

import br.com.marcus.pag.domain.usecase.FavoriteUseCase
import br.com.marcus.pag.presentation.main.favorite.viewmodel.FavoriteViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

internal val favoriteModule = module {
    factory { FavoriteUseCase(get()) }
    viewModel { FavoriteViewModel(get()) }
}

internal val provideFavoriteModule = loadKoinModules(favoriteModule)
fun loadFavoriteModule() = provideFavoriteModule