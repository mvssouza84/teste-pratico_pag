package br.com.marcus.pag.presentation.main.home.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.com.marcus.pag.R
import br.com.marcus.pag.data.model.GenreResult
import br.com.marcus.pag.data.model.Result
import br.com.marcus.pag.presentation.MovieResultListAdapter
import br.com.marcus.pag.presentation.base.BaseRecyclerAdapter
import br.com.marcus.pag.presentation.detail.view.DetailActivity
import br.com.marcus.pag.presentation.main.home.module.loadHomeModule
import br.com.marcus.pag.presentation.main.home.state.HomeState
import br.com.marcus.pag.presentation.main.home.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class HomeFragment : Fragment(), BaseRecyclerAdapter.OnItemClickListener {

    private val homeViewModel: HomeViewModel by viewModel()
    private val movieResultListAdapter by lazy { MovieResultListAdapter() }
    private lateinit var genreResult: GenreResult

    companion object {
        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        init()
    }

    override fun onItemClick(view: View, position: Int) {
        goToDetailActivity(movieResultListAdapter.recyclerList[position])
    }

    private fun init() {
        loadHomeModule()
        initObservers()
        setupRecycler()
        homeViewModel.loadGenres()
    }

    private fun goToDetailActivity(result: Result) {
        val intent = Intent(activity, DetailActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
        intent.putExtra("result", result)
        startActivity(intent)
    }

    private fun initObservers() {
        homeViewModel.homeState.observe(this, Observer { state ->
            when (state) {
                is HomeState.ShowLoading -> {
                    showLoading()
                }

                is HomeState.HideLoading -> {
                    hideLoading()
                }

                is HomeState.ShowSuccess -> {
                    showSuccessScreen(state.movieResult.results)
                    hideLoading()
                }

                is HomeState.SetGenreResult -> {
                    this.genreResult = state.genreResult
                    movieResultListAdapter.genreResult = this.genreResult
                }
            }
        })
    }

    private fun setupRecycler() {
        val linearLayoutManager = LinearLayoutManager(context)
        movieResultListAdapter.listener = this
        linearLayoutManager.orientation = RecyclerView.VERTICAL
        fragmentHomeRecycler.layoutManager = linearLayoutManager
        fragmentHomeRecycler.itemAnimator = DefaultItemAnimator()
        fragmentHomeRecycler.adapter = movieResultListAdapter
    }

    private fun showLoading() {
        fragmentHomeLoading.visibility = View.VISIBLE
        fragmentHomeRecycler.visibility = View.GONE
    }

    private fun hideLoading() {
        fragmentHomeLoading.visibility = View.GONE
    }

    private fun showSuccessScreen(results: List<Result>?) {
        fragmentHomeRecycler.visibility = View.VISIBLE
        movieResultListAdapter.addToList(results as ArrayList<Result>)
    }
}