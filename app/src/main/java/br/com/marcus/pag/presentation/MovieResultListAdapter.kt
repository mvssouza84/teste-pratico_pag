package br.com.marcus.pag.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.marcus.pag.BuildConfig
import br.com.marcus.pag.R
import br.com.marcus.pag.data.model.GenreResult
import br.com.marcus.pag.data.model.Result
import br.com.marcus.pag.extensions.formatDate
import br.com.marcus.pag.presentation.base.BaseRecyclerAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieResultListAdapter : BaseRecyclerAdapter<Result>() {

    var genreResult: GenreResult? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false), listener)
    }

    override fun getItemCount(): Int {
        return recyclerList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(recyclerList[position], genreResult)
    }

    class ViewHolder(
        private val view: View,
        private val listener: OnItemClickListener?
    ) : RecyclerView.ViewHolder(view) {

        fun bind(result: Result, genreResult: GenreResult?) = with(view) {
            Picasso.get().load(BuildConfig.IMAGE_URL + result.backdropPath).into(itemMovieImage)
            itemMovieTitle.text = result.title
            genreResult?.let {
                val genre = genreResult.genreList.single { g -> g.id == result.genreIds?.get(0) ?: 0 }
                itemMovieSubTitle.text =
                    resources.getString(
                        R.string.movie_subtitle,
                        result.releaseDate?.formatDate(),
                        genre.name
                    )
            } ?: run {
                itemMovieSubTitle.text =
                    resources.getString(
                        R.string.movie_subtitle,
                        result.releaseDate?.formatDate(),
                        ""
                    )
            }

            itemMovieContainer.setOnClickListener {
                listener?.onItemClick(view, adapterPosition)
            }
        }
    }
}