package br.com.marcus.pag.di.config

import androidx.room.Database
import androidx.room.RoomDatabase
import br.com.marcus.pag.data.dao.MovieDao
import br.com.marcus.pag.data.model.Result

@Database(entities = [Result::class], version = 1, exportSchema = false)
abstract class Database : RoomDatabase() {
    abstract fun movieDao(): MovieDao
}