package br.com.marcus.pag.presentation.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter<T> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var listener: OnItemClickListener? = null
    var recyclerList: ArrayList<T> = ArrayList()

    fun getList(): List<T>? {
        return recyclerList
    }

    @Synchronized
    fun clear() {
        val size = recyclerList.size
        if (size > 0) {
            recyclerList.clear()
            notifyItemRangeRemoved(0, size)
        }
    }

    fun addToList(list: ArrayList<T>) {
        recyclerList = list
        notifyDataSetChanged()
    }

    fun addItems(items: ArrayList<T>) {
        val lastPos = recyclerList.size - 1
        recyclerList.addAll(items)
        notifyItemRangeInserted(lastPos, items.size)
    }

    fun getItem(position: Int): T {
        return getList()!![position]
    }

    interface OnItemClickListener {
        fun onItemClick(view: View, position: Int)
    }
}