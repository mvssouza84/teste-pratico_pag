package br.com.marcus.pag.di

import br.com.marcus.pag.di.config.provideDataBaseBuilder
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

internal val dataBaseModule = module {
    single { provideDataBaseBuilder(androidContext()) }
}