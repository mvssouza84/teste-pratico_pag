package br.com.marcus.pag.di.config

import android.content.Context
import androidx.room.Room

fun provideDataBaseBuilder(context: Context) = Room.databaseBuilder(
    context,
    Database::class.java,
    "pag.db"
).build()
