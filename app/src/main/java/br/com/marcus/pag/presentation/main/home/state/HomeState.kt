package br.com.marcus.pag.presentation.main.home.state

import br.com.marcus.pag.data.model.GenreResult
import br.com.marcus.pag.data.model.MovieResult

open class HomeState {
    data class ShowSuccess(val movieResult: MovieResult) : HomeState()
    data class SetGenreResult(val genreResult: GenreResult) : HomeState()

    object ShowLoading : HomeState()
    object HideLoading : HomeState()
}