package br.com.marcus.pag.domain.usecase

import br.com.marcus.pag.data.model.GenreResult
import br.com.marcus.pag.data.model.MovieResult
import br.com.marcus.pag.domain.repository.GenreRepository
import br.com.marcus.pag.domain.repository.MovieRepository
import io.reactivex.Flowable

class HomeUseCase(private val movieRepository: MovieRepository, private val genreRepository: GenreRepository) {

    fun loadMovies(): Flowable<MovieResult> = movieRepository.loadMovies()

    fun loadGenres(): Flowable<GenreResult> = genreRepository.loadGenres()
}