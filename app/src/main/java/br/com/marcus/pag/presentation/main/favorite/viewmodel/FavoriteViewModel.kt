package br.com.marcus.pag.presentation.main.favorite.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.marcus.pag.domain.usecase.FavoriteUseCase
import br.com.marcus.pag.presentation.base.BaseViewModel
import br.com.marcus.pag.presentation.main.favorite.state.FavoriteState

class FavoriteViewModel(private val favoriteUseCase: FavoriteUseCase) : BaseViewModel() {

    private val _favoriteState by lazy { MutableLiveData<FavoriteState>() }

    val favoriteState: LiveData<FavoriteState>
        get() = _favoriteState

    fun loadFavorites() {
        _favoriteState.postValue(FavoriteState.ShowLoading)
        disposables.add(
            favoriteUseCase.loadFavorites().subscribe({
                _favoriteState.postValue(FavoriteState.HideLoading)
                if (it.isEmpty()) {
                    _favoriteState.postValue(FavoriteState.ShowEmptyState)
                } else {
                    _favoriteState.postValue(FavoriteState.ShowSuccess(it))
                }
            }, {
                _favoriteState.postValue(FavoriteState.HideLoading)
            })
        )
    }
}