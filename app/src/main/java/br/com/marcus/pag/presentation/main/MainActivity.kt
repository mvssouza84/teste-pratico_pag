package br.com.marcus.pag.presentation.main

import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import br.com.marcus.pag.R
import br.com.marcus.pag.presentation.base.BaseActivity
import br.com.marcus.pag.presentation.main.favorite.view.FavoriteFragment
import br.com.marcus.pag.presentation.main.home.view.HomeFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        initListeners()
        openHomeFragment()
    }

    private fun initListeners() {
        navigationView.setOnNavigationItemSelectedListener(this)
    }

    private fun openHomeFragment() {
        openFragment(HomeFragment.newInstance())
    }

    private fun openFavoriteFragment() {
        openFragment(FavoriteFragment.newInstance())
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.navigationHome -> {
                openHomeFragment()
            }
            R.id.navigationFavorite -> {
                openFavoriteFragment()
            }
        }
        return true
    }
}