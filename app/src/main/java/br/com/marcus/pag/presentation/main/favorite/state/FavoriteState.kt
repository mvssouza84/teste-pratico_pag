package br.com.marcus.pag.presentation.main.favorite.state

import br.com.marcus.pag.data.model.Result

sealed class FavoriteState {
    data class ShowSuccess(val resultList: List<Result>) : FavoriteState()

    object ShowLoading : FavoriteState()
    object HideLoading : FavoriteState()
    object ShowEmptyState : FavoriteState()
}