package br.com.marcus.pag.data.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import br.com.marcus.pag.data.model.Result
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface MovieDao {

    @Insert
    fun saveMovie(result: Result): Completable

    @Query("SELECT * FROM result WHERE id = :id")
    fun loadMovieById(id: Int?): Single<Result>

    @Delete
    fun deleteMovie(result: Result): Completable

    @Query("SELECT * FROM result")
    fun loadMovies(): Flowable<List<Result>>
}