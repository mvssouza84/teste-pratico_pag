package br.com.marcus.pag.presentation.detail.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.EmptyResultSetException
import br.com.marcus.pag.data.model.Result
import br.com.marcus.pag.domain.usecase.DetailUseCase
import br.com.marcus.pag.presentation.base.BaseViewModel
import br.com.marcus.pag.presentation.detail.state.DetailState

class DetailViewModel(private val detailUseCase: DetailUseCase) : BaseViewModel() {

    private val _detailState by lazy { MutableLiveData<DetailState>() }

    val detailState: LiveData<DetailState>
        get() = _detailState

    fun saveOrRemoveMovie(result: Result) {
        disposables.add(detailUseCase.getMovie(result).subscribe({
            deleteMovie(it)
        }, {
            if (it is EmptyResultSetException) {
                saveMovie(result)
            } else {
                _detailState.postValue(DetailState.ErrorGetLocalMovie)
            }
        }))
    }

    private fun saveMovie(result: Result) {
        disposables.add(detailUseCase.saveMovie(result).subscribe({
            _detailState.postValue(DetailState.SaveSuccess)
        }, {
            _detailState.postValue(DetailState.SaveError)
        }))
    }

    private fun deleteMovie(result: Result) {
        disposables.addAll(detailUseCase.removeMovie(result).subscribe({
            _detailState.postValue(DetailState.RemoveSuccess)
        }, {
            _detailState.postValue(DetailState.RemoveError)
        }))
    }
}