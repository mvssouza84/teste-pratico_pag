package br.com.marcus.pag.domain.repository

import br.com.marcus.pag.data.model.MovieResult
import br.com.marcus.pag.data.model.Result
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface MovieRepository {

    fun loadMovies(): Flowable<MovieResult>
    fun saveMovie(result: Result): Completable
    fun loadMovieById(id: Int?): Single<Result>
    fun deleteMovie(result: Result): Completable
    fun loadLocalMovies(): Flowable<List<Result>>
}