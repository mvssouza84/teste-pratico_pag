package br.com.marcus.pag.presentation.detail.state

sealed class DetailState {

    object SaveSuccess : DetailState()
    object SaveError : DetailState()
    object RemoveSuccess : DetailState()
    object RemoveError : DetailState()
    object ErrorGetLocalMovie : DetailState()
}