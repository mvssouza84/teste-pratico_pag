package br.com.marcus.pag.data.model


import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieResult(
    @field:Json(name = "dates") val dates: Dates?,
    @field:Json(name = "page") val page: Int?,
    @field:Json(name = "results") val results: List<Result>?,
    @field:Json(name = "total_pages") val totalPages: Int?,
    @field:Json(name = "total_results") val totalResults: Int?
) : Parcelable

@Parcelize
data class Dates(
    @field:Json(name = "maximum") val maximum: String?,
    @field:Json(name = "minimum") val minimum: String?
) : Parcelable

@Entity
@Parcelize
data class Result(
    @Ignore @field:Json(name = "adult") var adult: Boolean?,
    @field:Json(name = "backdrop_path") var backdropPath: String?,
    @Ignore @field:Json(name = "genre_ids") var genreIds: List<Int?>?,
    @PrimaryKey @field:Json(name = "id") var id: Int?,
    @field:Json(name = "original_language") var originalLanguage: String?,
    @field:Json(name = "original_title") var originalTitle: String?,
    @field:Json(name = "overview") var overview: String?,
    @field:Json(name = "popularity") var popularity: Double?,
    @field:Json(name = "poster_path") var posterPath: String?,
    @field:Json(name = "release_date") var releaseDate: String?,
    @field:Json(name = "title") var title: String?,
    @field:Json(name = "video") var video: Boolean?,
    @field:Json(name = "vote_average") var voteAverage: Double,
    @field:Json(name = "vote_count") var voteCount: Int
) : Parcelable {
    constructor() : this(false, "", null, 0, "", "", "", 0.0, "", "", "", false, 0.0, 0)
}