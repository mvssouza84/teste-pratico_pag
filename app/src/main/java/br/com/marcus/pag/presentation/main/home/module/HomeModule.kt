package br.com.marcus.pag.presentation.main.home.module

import br.com.marcus.pag.data.repository.GenreRepositoryImpl
import br.com.marcus.pag.data.repository.MovieRepositoryImpl
import br.com.marcus.pag.domain.repository.GenreRepository
import br.com.marcus.pag.domain.repository.MovieRepository
import br.com.marcus.pag.domain.usecase.HomeUseCase
import br.com.marcus.pag.presentation.main.home.viewmodel.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

internal val homeModule = module {
    factory<MovieRepository> { MovieRepositoryImpl(get(), get()) }
    factory<GenreRepository> { GenreRepositoryImpl(get()) }
    factory { HomeUseCase(get(), get()) }
    viewModel { HomeViewModel(get()) }
}

internal val provideHomeModule = loadKoinModules(homeModule)
fun loadHomeModule() = provideHomeModule