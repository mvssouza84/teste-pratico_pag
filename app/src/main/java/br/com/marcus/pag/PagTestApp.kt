package br.com.marcus.pag

import android.app.Application
import br.com.marcus.pag.di.dataBaseModule
import br.com.marcus.pag.di.netWorkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class PagTestApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@PagTestApp)
            modules(listOf(netWorkModule, dataBaseModule))
        }
    }
}