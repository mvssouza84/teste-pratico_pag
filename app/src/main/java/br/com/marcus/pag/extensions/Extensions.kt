package br.com.marcus.pag.extensions

import java.text.SimpleDateFormat
import java.util.*

private val fromDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
private val toDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())

fun String.formatDate(): String? {
    return fromDate.parse(this)?.let { date ->
        toDate.format(date)
    }
}