package br.com.marcus.pag.di

import br.com.marcus.pag.di.config.provideApi
import br.com.marcus.pag.di.config.provideInterceptor
import br.com.marcus.pag.di.config.provideMoshi
import br.com.marcus.pag.di.config.provideOkHttpClientBuilder
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module

val netWorkModule = module {
    single { provideApi() }
}