package br.com.marcus.pag.presentation.main.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.marcus.pag.domain.usecase.HomeUseCase
import br.com.marcus.pag.presentation.base.BaseViewModel
import br.com.marcus.pag.presentation.main.home.state.HomeState

class HomeViewModel(private val homeUseCase: HomeUseCase) : BaseViewModel() {

    private val _homeState by lazy { MutableLiveData<HomeState>() }

    val homeState: LiveData<HomeState>
        get() = _homeState

    fun loadGenres() {
        _homeState.postValue(HomeState.ShowLoading)
        disposables.add(
            homeUseCase.loadGenres()
                .subscribe({
                    _homeState.postValue(HomeState.SetGenreResult(it))
                    loadMovies()
                }, {
                    _homeState.postValue(HomeState.HideLoading)
                })
        )
    }

    private fun loadMovies() {
        disposables.add(
            homeUseCase.loadMovies()
                .subscribe({
                    _homeState.postValue(HomeState.HideLoading)
                    _homeState.postValue(HomeState.ShowSuccess(it))
                }, {
                    _homeState.postValue(HomeState.HideLoading)
                })
        )
    }
}