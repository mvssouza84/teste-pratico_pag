package br.com.marcus.pag.data.repository

import br.com.marcus.pag.BuildConfig
import br.com.marcus.pag.data.api.Api
import br.com.marcus.pag.data.model.GenreResult
import br.com.marcus.pag.domain.repository.GenreRepository
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GenreRepositoryImpl(private val api: Api) : GenreRepository {

    override fun loadGenres(): Flowable<GenreResult> {
        return api
            .loadGenres(BuildConfig.API_KEY)
            .observeOn(Schedulers.io())
            .subscribeOn(AndroidSchedulers.mainThread())
    }
}